# Best practices

There is no such thing as Best practices. Nothing can be best for everyone, everywhere. 
This repo can therefore only be improved to become better than before. And it's already a lot

Also don't miss
* [CI best practices](https://code.europa.eu/digit-c4/dev/ci-best-practices)
* [Python best practices](https://code.europa.eu/digit-c4/dev/python-best-practices)
* ...
and many more. See the [full list usint the `best-practices` topic](https://code.europa.eu/explore/projects/topics/best-practices)

## Local Workspace
> Git tracked directories shouldn't be backup-ed using
> another tool. All projects should therefore be stored
> out of the scope of OneDrive or other similar cloud storage

> Local project clones should reflect remote repository grouping and naming

**=> Projects must be cloned in `~/Workspace/${REGISTRY_HOST}/${REPOSITORY_PATH}`**

Example local file organization
```
~/Workspace               # <== directly in Windows/linux home dir
| - hg.***.int/           # <== category = folder
| | - opt/SNet/scm/opt
| | | - opt_lib           # <== a repository in the legagy git
| ...
| - sdlc.***.europa.eu/
| | - c4/
| | | - shared/
| ...
| – code.europa.eu/       # <== hostname of the registry
| | - digit-c4/           # <== gitlab group = folder
| | | - dev/
| | | | best-practices/   # <== this repository, without *.git
| ...
```
```shell
PROJECT_HOME="~/Workspace/code.europa.eu/digit-c4/dev/best-practices"
mkdir -p $(dirname $PROJECT_HOME) 
git clone git@code.europa.eu:digit-c4/dev/best-practices.git $PROJECT_HOME \
  && cd $PROJECT_HOME
```
Also don't forget to sync gitmodules
```shell
git submodule update --init --recursive
```

### HTTPS Gitlab git credentials
If HTTPS must be used for Gitlab, the first step is
to create a Personal access token in the [user settings](https://code.europa.eu/-/user_settings/personal_access_tokens).
It must have
* read_repository
* write_repository (if the goal is to allow push)

Keep the token in `$GITLAB_PERSONAL_AT` variable
```bash
# Fill in Gitlab full domain name
GITLAB_FQDN=code.europa.eu
# Enable git credential storage
git config --global credential.helper store
# Register association between gitlab FQDN, username, and personal access token
echo "https://${USER}:${GITLAB_PERSONAL_AT}@${GITLAB_FQDN}" >> ~/.git-credentials
```

### Migrate existing repo to Gitlab
Because Gitlab can be checking commit authorship, it may be not possible to push an existing project history.
> ```
> remote: GitLab: You cannot push commits for 'another.user@ext.ec.europa.eu'. You 
> can only push commits if the committer email is one of your own verified emails.
> ```

#### Alternative #1 — full history squash
The First alternative is to create a new orphan branch.
> ⚠️ **WARNING** ⚠️ 
> 
> you will loose all change history

```shell
# locally clone and move to the repo to migrate
git clone origin_repo.git && cd origin_repo
# checkout the commit to migrate
git checkout main
# Create an orphan branch from that point
git checkout --orphan sqhashed-main
# Bundle everything into a new commit
git add --all
git commit -m "Squash main history"
# Add the gitlab remote
git remote add gitlab https://code.europa.eu/digit-c4/dev/best-practices.git
# And push the squashed main to the main branch of the new Gitlab
git push gitlab squashed-main:main
```

#### Alternative #2 — authorship metadata overwrite
If the history must be preserved a solution is to only overwrite the author email
> ⚠️ **WARNING** ⚠️
>
> This is going to alter the hash code of all commits and will create a whole
> new history tree

![git-authorship-overwrite.drawio.png](doc/git-authorship-overwrite.drawio.png)
```shell
# Locally clone the repository to squash
git clone git@code.europa.eu:digit-c4/dev/best-practices.git \
  && cd best-practices
# synchronise all remote branches with local
for remote in `git branch -r | grep -v HEAD`; do git branch --track ${remote#origin/} $remote; done
# Use the nuclear-bomb `filter-branch` to rewrite history
EC_EMAIL_ADDRESS=raphael.joie@ext.ec.europa.eu
git filter-branch --commit-filter '
  GIT_AUTHOR_NAME="Rebased history. Initial author was \"${GIT_AUTHOR_NAME}, ${GIT_AUTHOR_EMAIL}\""
  GIT_AUTHOR_EMAIL='${EC_EMAIL_ADDRESS}'
  GIT_COMMITTER_NAME="Rebased history. Initial committer was \"${GIT_COMMITTER_NAME}, ${GIT_COMMITTER_EMAIL}\""
  GIT_COMMITTER_EMAIL='${EC_EMAIL_ADDRESS}'
  git commit-tree "$@";
' -- --all
# Remove former origin
git remote rm origin
# Declare new origin, and push everything
git remote add origin https://other.domain/digit-c4/dev/best-practices.git
git push --all
git push --tags
```

## Versioning and branching
Code change lifecycle often lack of governance or is too rigid to be applicable.
We acknowledge the need to provide strong but flexible framework or this section 
may turn to become useless

### I. Versioning
Versioning strategy too often looks like a blind opt in for [semantic versioning](https://semver.org/)
while the choice comes with side effects
and hidden responsibilities. Pointing at an existing well-documented strategy is an easy win for software architects but 
unfortunately comes at a cost for the implementation team. We therefore strongly advise product owners to challenge
the technical leaders to check for a potential simpler approach.


#### Problem 1: exponential responsibility growth
Because SemVer is not linear, each new *minor* or *major* version open a new branch in the version tree. Each version 
branch must either
* be maintained
* or explicitly discontinued

![semver.drawio.png](doc%2Fsemver.drawio.png)

Example: a security fix on version 0.4.12 doesn't mean one, but four theoretical updates. Each minor must be patched.
Unless the maintainer documented a version termination for each minor.

That responsibility is highly theoretical because the vast majority of the projects following the semantic versioning 
will **NEVER** fix any past version. The analysis of [PyPi stats](https://pypistats.org/) indeed shows that most 
projects will follow a pure linear development. It will never make use of the parallel versioning feature provided by
semantic versioning. 

> CONCLUSION: This lack of support shows that the version parallelization is actually useless for most of us. Most
> libraries will only patch the latest version. Most users will be ok if patch doesn't exist for an older version.

> CONSEQUENCE: only use semantic versioning if 
> 1. Users are requesting long time release (LTR).
> 2. you have the means to follow up (close or fix) on parallel versions.

#### Problem 2: discouraged continuous adoption
According to semantic versioning, breaking changes should be gathered in major changes. But that approach is
aging. With the rise of continuous processes (integration, deliver, improvements, etc.), it is a bad practice to
wait for big changes to pack before it is released.

Semantic versioning is often presented as a solution to breaking changes while it is actually worsening it. Instead of
promoting the collection of breaking changes, code owners should opt for a versioning strategy that is promoting
the diffusion of smaller breaking changes in the time.

> CONCLUSION: Gathering breaking changes in big bang upgrade will actually make the first problem even worse.
> Because users willing to stick with an old feature set are supported not to continuously adopt change. 

> CONSEQUENCE: consider the use of a versioning strategy that is promoting continuous adoption.

#### Solution
1. always start projects using build versioning
   > (*) *Build Versioning* is the labelling of code using only a simple integer, eventually using
   > a temporal prefix (year, generation number, etc.) that has no functional meaning. Examples of that strategy:
   > Google Chrome, IntelliJ applications, Adobe, etc.
   ```
   b1
   b1.1
   b2
   b3
   ...
   ```

   Because most package managers are using semantic versioning, build version can always be
   translated into semver.
   ```
   b1 = v0.1.0
   b1.1 = v0.1.1
   b2 = v0.2.0
   ```
   
   the alternative is the [ZeroVer](https://0ver.org/) that is a build versioning but with explicit
   SemVer compatibility.

2. Breaking and non-breaking changes are allowed any time (as it is anyway already the case for v0.x versions in Semver). Stop
   piling technical debt because you have a "responsibility" towards your community. This is only true for commercial
   library with millions of users. Your responsibility is to iterate fast and everything else is in the hand of the 
   users who must carefully consider the version to use, no matter the major or minor.

   this approach will also incentivise the users to always get the latest features as it is the only way to get
   the latest security patches.

3. Switch to semantic versioning when it is *really* needed, and the project has enough traction to cover the
   over cost of maintenance.

   **Recall that Semantic versioning is never needed when**
    * the code is an application that is deployed by the owner of the code (SaaS). Only the (API/GUI) of the application
      may require a versioning, but not the application itself!
    * the code is an application installed on many uncontrolled hosts but the owner wants to enforce users to implement
      continuous upgrades.

    When opting for Semantic versioning, make sure there is a restricted definition of what is being maintained. Or the
    entire set of versions is supposed to be continued. LTR is a way to tackle it but is out of scope of this document.

### II. Version synchronisation between tags, code, and artifact metadata
Maintaining version number in the code base is often seen as a tedious job,
especially for developers. It is unclear what number to increase, when, where?
The outcome may either be 
* a de-synchronisation between code and published version
* or the re-publishing of a same lib version but not the same implementation

The underlying question is
> Should version number be kept 
> 1. in code (in a tracked file), 
> 2. git tag 
> 3. and/or build metadata ? 
>
> What is the primary location, and how to ensure synchronisation?

To avoid such a mess, the motivation for each option must be clarified

**#1 artefact versioning** — Developers and code do not care about self version number. A developer's toolset or techniques
doesn't change based on odd or even version number. The versioning is only useful once a code has been frozen and
distributed. The consequence is that we should always recall that versioning priority #1 is delivery, not integration.

**#2 code base versioning bookmark** — version number may still be useful at implementation time. Someone way want 
to inspect the underlying raw code of a given bundled version. 
The code base should therefore be tagged **after** a build is delivered. 
It is a bookmark to get back to a previously identified point in time.

**#3 code extract identity** — Storing version number in the code can eventually help to understand whether a code base extract
is up-to-date, or not. Also, when an archive has been created from a development branch, it can help to find
out where the divergence with main version started. But all that information (and much more) can be found using `git` commands.
The version in code is therefore only useful when it was taken out of git. It is a duplicate otherwise.

#### => Conclusion
Priority for version is
1. (mandatory) as build metadata (the primary version data source)
2. (mandatory) as commit tag, to bookmark a given build
3. (optional) as hardcoded value, as a mirror of the git metadata in case code leaves git

> **NB about hardcoded value**
>
> please review programming language specific best practices.

### III. Release and dev strategy
Git flow is again another risk-less and brain-less solution that is coming with a lot of extra weight
at implementation time. It suggests the use of intermediate `release-X` and `dev` branches but most vendors (Gitlab [here](https://about.gitlab.com/blog/2020/03/05/what-is-gitlab-flow/), and GitHub [there](https://docs.github.com/en/get-started/using-github/github-flow))
are promoting a more applicable solution.

#### Release-X branches
The creation of a `release-X` branch before features are merged into `main` comes at the cost of making the maintenance 
process heavier, potentially involving organisational blockers if the `release-X` branch was locked. Better to 
simplify the process whenever possible and only enable release branch when needed.

1. If there is one single feature to merge: *Alternative*: Pull `main` in the feature branch before to open a merge request.
2. If there are multiple short lasting feature: *Alternative*: Use thunk strategy and merge each one individually

Feature branch only makes sense if there are numerous long-lasting features to bundle together.
The `release-X` branch will therefore help to bring the features together with main before it is merged.

#### Dev branch
Same goes for `dev` branch. Such an intermediary step completely discard any thunk based fast iterative approach. It may
only be useful for repos involving hundreds of feature merge. The main branch of this kind of repo may indeed become too
long. The use of a dev branch will help to summarize the main history, opening the door to performance optimization.

![dev-branch.drawio.png](doc%2Fdev-branch.drawio.png)

But it is completely useless to normal repos. Such ultra popular project is out of scope of this document. And we
discourage the use of that `dev` branch

#### Conclusion
1. diverge from main branch to work on features
   ```shell
   # Load latest from main
   git checkout main && git pull
   # And fork from latest in main
   git checkout -b new-feature-branch 
   ```
2. Develop features and don't bother about any hardcoded version reference. It must stay as it is because it helps to
   get back to `main` root commit in case of git extract.
   ```shell
   echo "# a change" >> scrypt.py
   ```
3. Ask the project owner to pick a new version number based on the nature of the changes, 
   and make sure the feature is still line with `main`
   ```shell
   # pull latest main version
   git checkout main && git pull
   git checkout new-feature-branch
   git merge main
   ```
   or gather features branches into a release
   ```shell
   # Load latest from main
   git checkout main && git pull
   # Fork from that latest
   git checkout -b release-me
   # And pull features changes in release
   git merge new-feature-branch
   git merge other-feature-branch
   ```

4. Bundle a "rc" (Release candidate) version from the feature or release branch, labelled
   after the previously picked version number, and tag the commit accordingly
   ```shell
   git checkout new-feature-branch
   # or
   git checkout release-me

   build-script.sh --version v0.1.4-rc.1
   git tag v0.1.4-rc.1
   ```
   NB: as tags are mutable, it can potentially be used as trigger point, and assigned *before* the actual build.
   **Just be sure to remove the given tag if the build fails or if nothing was published.**

5. Once a release build is successful and tested, it can be published in production.
   > ⚠️ **WARNING** ⚠️ Build common mistake
   >
   > Bundled code **SHOULDN'T BE** re-build at publishing time. But a previously published RC version should instead
   > be copy-pasted as the new version. A build is always time sensitive, no matter the quality of the build pipeline.
   > When something is ready to go, don't suddenly re-build it. Instead, take the tested and validated
   > build and move it to the next stage as is. 
   >
   > This is true for any artefact type, library, application, container, etc. 

   ```shell
   # Script taking bundled version v0.1.4-rc.3 from artifact registry
   # And cloning it into v0.1.4
   publish-script.sh --version v0.1.4 --from-version v0.1.4-rc.3
   ```

   Also, the release branch must merge to main once it is published. A hardcoded version may be pushed in
   the code during the merge.
   ```shell
   git checkout new-feature-branch
   # or
   git checkout release-me
   
   git tag v0.1.4
   
   echo "v0.1.4" > VERSION
   git add VERSION && git commit -m "bump version v0.1.4"
   git checkout main && git merge release-me
   ```

### IV. Summary
![branching-build.drawio.png](doc%2Fbranching-build.drawio.png)

*First and simplest branching strategy, strongly promoted for early projects*

![branching-build-release.drawio.png](doc%2Fbranching-build-release.drawio.png)

*Same versioning but with release branch when needed*

![branching-semantic.drawio.png](doc%2Fbranching-semantic.drawio.png)

*Impact of semver on branching. Version branches virtually stays open until it is closed, and fixes must apply to all open versions.*



## Logging & metrics

### Output
Applications should be generating logs to standard error pointer (STDERR).
It is not part of any application to forward logs to files or database. 
The transport and storage of logs is an ops and infrastructure responsibility.

### Verbosity
The verbosity level should spread across 5 levels
1. **Debug**: Diagnostic information used for troubleshooting.
2. **Info**: normal behaviour events. Used for metrics and performance indicator
3. **Warn**: runtime situation only problematic when it happens often
4. **Error**: runtime issue requiring intervention either immediately, or in the near future
5. **Fatal**: severe runtime issue with premature termination.

And the application level should be configured using the `LOG_LEVEL` environment variable,
combined with the uppercase log level name
```bash
export LOG_LEVEL=INFO
```

### Format and content
* log should be one-lined to ease syslog parsing. Multiple line message should be printed displaying `\n` strings instead of new lines
* Promote structured, or at least, semi structured messages. Unstructured data should be at the end of the line.
* Each line should start with standard information, named the log header
  1. host name to identify the machine generating the message. NB: the host name may be prepended to all lines by the log collector agent.
  2. timestamp in ISO format
  3. the process ID
  4. the logger name. Most programming languages and frameworks support multiple parallel loggers, potentially involving distinct formatting. Everything before the logger name marker
    should be standard. Only the remaining string may vary
  5. the log level
  6. the owner organisation unit name and type (team, squad, unit, etc)
  7. the application name and type (api, gui, task, playbook, etc)
* The log header can use condensed formatting because it is structured,
  standardised, and will repeat itself often. The accumulation of special chars separator should still be avoided
  to ease the later parsing. Ordered information between brackets are to be promoted.
* consider visual padding for header blocks designed to be displayed in the same stream. Example: `00123` for the PID instead of `123` and `[INFO ]` for the log level
* The remaining non-standard structured information should use a more verbose formatting
    * key-value: for simple data. Example: `status=200 method=POST path=/my/endpoint`
    * json: for complex data
* unstructured information at the end of the message, after a special character that is elsewhere escaped. Example `tab`.

> **Example of log header**
> ```log
> [2023-02-14T12:41:23.125Z][00124][logger-name][INFO ][squad:dev][api:hermes] key=value   unstructured message
> [2023-02-15T21:41:12.531Z][00042][logger-name][DEBUG][squad:dev][api:hermes] key=12 unstructured message
> [2023-02-15T21:43:43.024Z][01493][logger-name][WARN ][squad:dev][api:hermes] key="value2\twith tab"  unstructured message
> ```
> * [ ] host name missing at the beginning of the line because it is automatically added by the log collector
> * [x] partially structured message
> * [x] standard information. Host name skipped because it is to be added by the log collector
> * [x] log header condensed
> * [x] first log headers including padding to ease visual reading
> * [x] remaining non-standard optional value formatted using k=v
> * [x] unstructured information at the end of the message after the `tab` character.

### Known libraries
The dev team is maintaining a set of libraries to standardise the logging output
* [Python NMS Common](https://code.europa.eu/digit-c4/dev/python-nms-common/)
* [Perl NMS Common](https://code.europa.eu/digit-c4/dev/perl-nms-common)

## Inspiring trainings, documents, videos, etc
* [Static, Unit, Integration, and End-to-End Tests Explained](https://www.youtube.com/watch?v=TLccnKIMggA)

## Gitlab pipelines
This repository also provides useful Gitlab CI pipelines

### Smithy
```yaml
doc-smithy-job:
  tags:
    - docker
  image: code.europa.eu:4567/digit-c4/dev/best-practices/smithy:latest
  script:
    - smithy build
``` 

### Secret scanning
```yaml
include:
  - project: 'digit-c4/dev/best-practices'
    file: 'gitlab-ci/test-static-scan.yml'
    ref: v1.0
  
stages:
  - test

test-static-scan-gitleaks:
  stage: test
  variables:
    #SRC_PATH: "./"
```

## Contributing
The "Best practices" project is about
* **A/** best practices documentation: as markdown documentation files in `X-best-practices` repositories. Each tech have their
  own `X-best-practices` repository, on top of this generic one that applies to all
* **B/** collection of shared piece of code: as part of the `X-nms-common` repository, or standalone. It is a gathering from
  the smallest snippet to the fully standalone library, designed to be reused in projects. By design any shared piece of
  code should follow and ease the implementation of what has been described in `X-best-practices`
* **C/** collection of standardised Gitlab-CI jobs: as `yml` files in `/gitlab-ci` directory of the `X-best-practices` projects.
  See [CI Best practices](https://code.europa.eu/digit-c4/dev/ci-best-practices)

The lifecycle of **A/** and **B/** is as follows:
1. The input comes from
   * Document from compliance = high level contract requirement
   * Lesson learned from a R&D project, PoC, experience, etc.
2. Structure into x-Best practice.
3. Eventually distribute a solution implementation in the X-nms-common project
   1. Example snippet
   2. Boilerplate
   3. Template
   4. Library, distributed via git.
      ```shell
      pip install git@code.europa.eu:digit-c4/dev/perl-nms-common.git
      ```
   5. Library, distributed with Gitlab package registry
      ```toml
      # pyproject.toml
      [[tool.poetry.source]]
      name = "code.europa.eu"
      url = "https://code.europa.eu/api/v4/projects/digit-c4%2Fdev%2Fnetbox-nms-common/packages/pypi/simple"
      priority = "explicit"
   
      [tool.poetry.dependencies]
      netbox-nms-common = {version = "==0.3.0", source = "code.europa.eu"}
      ```
4. The `x-nms-common` is an "incubator" for shared code content. If a module becomes too important,
   it should be extracted from x-nms-common as standalone library with its own lifecycle.

The lifecycle of **C/** is as follow
1. Find project with existing .gitlab-ci, but hardcoded `script` job section
2. identify the related `x-best-practices` project, create the equivalent job template
3. Optionally create a dedicated Docker image for the job template
4. Create a merge request in the initially identified project to use the newly created job template

NB: Step 1 could also be to identify projects without `.gitlab-ci.yml`, and suggest existing/new pipelines to implement
