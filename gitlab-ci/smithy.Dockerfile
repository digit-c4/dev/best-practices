FROM python:3.11

RUN mkdir -p smithy-install/smithy \
    && curl -L https://github.com/smithy-lang/smithy/releases/download/1.43.0/smithy-cli-linux-x86_64.tar.gz -o smithy-install/smithy-cli-linux-x86_64.tar.gz \
    && tar xvzf smithy-install/smithy-cli-linux-x86_64.tar.gz -C smithy-install/smithy \
    && smithy-install/smithy/install \
    && rm -rf smithy-install
