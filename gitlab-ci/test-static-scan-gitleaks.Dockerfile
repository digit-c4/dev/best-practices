FROM golang:1.22.0-bookworm

RUN apt-get install -y git --no-install-recommends
RUN git clone https://github.com/gitleaks/gitleaks.git \
  && cd gitleaks && make build \
  && mv gitleaks /usr/local/bin/ \
  && cd ../ \
  && rm -rf gitleaks

#RUN gitleaks version
